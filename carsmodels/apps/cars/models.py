from django.db import models


class CarBrand(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name


class CarModel(models.Model):
    name = models.CharField(max_length=30)
    brand = models.ForeignKey(CarBrand, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.brand.name} - {self.name}"
