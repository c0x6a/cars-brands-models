from django.shortcuts import render
from django.views.generic import TemplateView

from apps.cars.forms import CarBrandForm, CarModelForm
from apps.cars.models import CarModel


class AvailableCars(TemplateView):
    template_name = 'cars.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data()
        brands_form = CarBrandForm(prefix='brand', data=request.GET or None)
        models_form = CarModelForm(prefix='model', data=request.GET or None)

        car_brand = request.GET.get('brand-name')
        if car_brand:
            models_form.fields['name'].queryset = CarModel.objects.filter(brand=car_brand)

        context.update({
            'brands_form': brands_form,
            'models_form': models_form,
        })
        return render(request, self.template_name, context)
