from django import forms

from apps.cars.models import CarBrand, CarModel


class CarBrandForm(forms.ModelForm):
    name = forms.ModelChoiceField(
        queryset=CarBrand.objects.all(),
        widget=forms.Select(attrs={'class': 'form-control'}),
    )

    class Meta:
        model = CarBrand
        fields = ('name',)


class CarModelForm(forms.ModelForm):
    name = forms.ModelChoiceField(
        queryset=CarModel.objects.none(),
        widget=forms.Select(attrs={'class': 'form-control'}),
    )

    class Meta:
        model = CarModel
        fields = ('name',)
