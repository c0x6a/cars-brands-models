from django.urls import path

from apps.cars.views import AvailableCars

app_name = 'cars'

urlpatterns = [
    path('', AvailableCars.as_view()),
]
