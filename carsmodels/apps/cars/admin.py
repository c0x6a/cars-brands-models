from django.contrib import admin

from apps.cars.models import CarBrand, CarModel


class CarBrandsAdmin(admin.ModelAdmin):
    class Meta:
        model = CarBrand

    fields = ('name',)
    list_display = ('name',)


class CarModelsAdmin(admin.ModelAdmin):
    class Meta:
        model = CarModel

    fields = ('brand', 'name',)
    list_display = ('brand', 'name',)


admin.site.register(CarBrand, CarBrandsAdmin)
admin.site.register(CarModel, CarModelsAdmin)
