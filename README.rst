==========
CarsModels
==========

A simple django project to change values from a dropdown based on the
value of a previous dropdown selected value.

Just an example for someone that
`asked in for it <https://t.me/django/41850>`_
in the `@Django <https://t.me/django>`_'s telegram group.


How to run:
-----------

#. Create a virtual environment, install dependencies from
   ``requirements.txt``

#. Run migrations: ``./manage.py migrate``.

#. Load fixtures:

    - ``./manage.py loaddata carbrand``
    - ``./manage.py loaddata carmodel``

#. Run ``./manage.py runserver`` inside the ``carsmodel`` folder, just
   as any other simple django project and open a web browser.
